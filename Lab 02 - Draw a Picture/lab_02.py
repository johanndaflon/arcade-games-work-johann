import arcade

arcade.open_window(600, 600, "Drawing Example")

arcade.set_background_color(arcade.color.LAVENDER_MAGENTA)
arcade.start_render()
arcade.draw_lrtb_rectangle_filled(30, 200, 100, 80, (0, 0, 0))
arcade.finish_render()
arcade.run()